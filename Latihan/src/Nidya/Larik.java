package Nidya;

//import latihan.Mahasiswa;
public class Larik {

    public static void cetak(int[] x) {
        for (int i = 0; i < x.length; i++) {
            System.out.print(x[i] + " ");
        }
    }

    public static void cetak(Object[] n) {
        for (int k = 0; k < n.length; k++) {
            System.out.println(n[k] + " ");
        }
    }

    public static void cetak(int[][] n) {
        for (int i = 0; i < n.length; i++) {
            for (int j = 0; j < n[i].length; j++) {
                System.out.print(n[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void cetak(Object[][] n) {
        for (int i = 0; i < n.length; i++) {
            for (int j = 0; j < n[i].length; j++) {
                System.out.print(n[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static int SequentialSearch(int[] larik, int kunci) {
        // Langkah 2 : Untuk counter = 0 sampai N-1 lakukan langkah 3
        for (int counter = 0; counter < larik.length; counter++) {
            /* Langkah 3 : Test apakah kunci == larik[counter] Jika ya, 
            kembalikan nilai counter*/
            if (larik[counter] == kunci) {
                return counter;
            }
        }
        /* Langkah 4 : Kembalikan nilai -1 untuk menunjukan data 			   
        tidak ditemukan */
        return -1;
    }

    public static int SequentialSearch(String[] larik, String kunci) {
        for (int counter = 0; counter < larik.length; counter++) {
            if (larik[counter].compareTo(kunci) == 0) {
                return counter;
            }
        }
        return -1;
    }

    public static int SequentialSearch(Object[] larik, Object kunci) {
        for (int counter = 0; counter < larik.length; counter++) {
            if (((Comparable) larik[counter]).compareTo(kunci) == 0) {
                return counter;
            }
        }
        return -1;
    }
    
    public static int binarySearch(int[] larik, int x) {
        /* Lanjutan Langkah 0 : indekAwal diisi indek data pertama dan indekAkhir 
        diisi indek data terakhir*/
        int indeksAwal, indeksAkhir;
        indeksAwal = 0;
        indeksAkhir = larik[larik.length - 1];
        // Langkah 2 :Selama (indekAwal <= indekAkhir) lakukan langkah: 3, 4, 5
        while (indeksAwal <= indeksAkhir) {
            /* Langjah 3 : Carilah elemen tengah. Elemen tengah adalah elemen 
               dengan indekTengah = (indekAwal+indekAkhir) /2. 
	      (Elemen tengah, larik[indekTengah], membagi larik menjadi dua bagian, 
               yaitu bagian kiri larik[indekAwal .. indekTengah-1] dan 
               bagian kanan larik[indekTengah+1 .. indekAkhir])*/
            int indeksTengah = (indeksAwal + indeksAkhir) / 2;
            // Langkah 4:Test apakah larik[indekTengah] = X. 
            if (larik[indeksTengah] == x) {
                /*Jika ya, pencarian dihentikan sebab X sudah ditemukan dan kembalikan 
                nilai indekTengah.*/
                return indeksTengah;
                // Jika tidak, lanjutkan  ke langkah 5.
            } else {
                /*Langkah 5: Harus ditentukan apakah pencarian selanjutnya akan 
             dilakukan di larik bagian kiri atau di bagian kanan. 
             Test larik[indekTengah] > X*/
                if (larik[indeksTengah] > x) {
                    /*  Jika ya pencarian selanjutnya dilakukan di larik bagian 
                        kiri,  buat indekAkhir=indekTengah-1.*/
                    indeksAkhir = indeksTengah - 1;
                    /*  Jika tidak pencarian selanjutnya dilakukan pada larik 
                        bagian kanan*/
                } else {
                    // buat indekAwal=indekTengah+1.
                    indeksAwal = indeksTengah + 1;
                }
            }
        }
        // Langkah 6: Kembalikan nilai -1 untuk menunjukkan data tidak ditemukan.
        return -1;
    }
    
    public static int binarySearch(Object[] larik, Object x) {
        /* Lanjutan Langkah 0 : indekAwal diisi indek data pertama dan indekAkhir 
        diisi indek data terakhir*/
        int indeksAwal, indeksAkhir;
        indeksAwal = 0;
        indeksAkhir = larik.length - 1;
        // Langkah 2 :Selama (indekAwal <= indekAkhir) lakukan langkah: 3, 4, 5
        while (indeksAwal <= indeksAkhir) {
            /* Langjah 3 : Carilah elemen tengah. Elemen tengah adalah elemen 
               dengan indekTengah = (indekAwal+indekAkhir) /2. 
	      (Elemen tengah, larik[indekTengah], membagi larik menjadi dua bagian, 
               yaitu bagian kiri larik[indekAwal .. indekTengah-1] dan 
               bagian kanan larik[indekTengah+1 .. indekAkhir])*/
            int indeksTengah = (indeksAwal + indeksAkhir) / 2;
            // Langkah 4:Test apakah larik[indekTengah] = X. 
            if (((Comparable)larik[indeksTengah]).compareTo(x) == 0) {
                /*Jika ya, pencarian dihentikan sebab X sudah ditemukan dan kembalikan 
                nilai indekTengah.*/
                return indeksTengah;
                // Jika tidak, lanjutkan  ke langkah 5.
            } else {
                /*Langkah 5: Harus ditentukan apakah pencarian selanjutnya akan 
             dilakukan di larik bagian kiri atau di bagian kanan. 
             Test larik[indekTengah] > X*/
                if (((Comparable) larik[indeksTengah]).compareTo(x) > 0) {
                    /*  Jika ya pencarian selanjutnya dilakukan di larik bagian 
                        kiri,  buat indekAkhir=indekTengah-1.*/
                    indeksAkhir = indeksTengah - 1;
                    /*  Jika tidak pencarian selanjutnya dilakukan pada larik 
                        bagian kanan*/
                } else {
                    // buat indekAwal=indekTengah+1.
                    indeksAwal = indeksTengah + 1;
                }
            }
        }
        // Langkah 6: Kembalikan nilai -1 untuk menunjukkan data tidak ditemukan.
        return -1;
    }

//    public static int interpolation(int[] larik, int kunci) {
//        int indeksAwal, indeksAkhir;
//        indeksAwal = 0;
//        indeksAkhir = larik[larik.length - 1];
//        while (indeksAwal <= indeksAkhir) {
//            for (int i = 0; i < larik.length; i++) {
//                // mid = low + ((toFind - sortedArray[low]) * (high - low)) /(sortedArray[high] - sortedArray[low]);
//
//                 int indeksTengah = indeksAwal + indeksAkhir;
//            }
//           
//        }return -1;
//    }
}
