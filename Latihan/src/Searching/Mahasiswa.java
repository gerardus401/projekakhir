package Searching;

public class Mahasiswa implements Comparable {

    private String nama;
    private int nim;

    public Mahasiswa() {
    }

    public Mahasiswa(String nama) {
    }

    public Mahasiswa(int nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    @Override
    public String toString() {
        return "Nama Mahasiswa : " + nama + "\tNIM : " + nim + "\t";
    }

//    @Override
//    public int compareTo(Object o) {
//        // Mahsiswa mhs = (Mahasiswa)o;
//        if (nim == ((Mahasiswa) o).nim) {
//            return 0;
//        } else if (nim > ((Mahasiswa) o).nim) {
//            return 1;
//        } else{
//            return -1;
//        }
//    }
    @Override
    public int compareTo(Object o) {
        // Mahsiswa mhs = (Mahasiswa)o;      
//        if (nama == ((Mahasiswa) o).nama) {
//            return 0;
//        }
//            return -1;return nama.compareTo(((Mahasiswa)o).nama);
        return nama.compareTo(((Mahasiswa) o).nama);
    }
}
