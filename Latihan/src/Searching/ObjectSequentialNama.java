package Searching;

import Nidya.Larik;

public class ObjectSequentialNama {

    public static void main(String[] args) {
        Mahasiswa mhs[] = new Mahasiswa[5];
        mhs[0] = new Mahasiswa(105);
        mhs[0].setNama("Agit");
        mhs[1] = new Mahasiswa(103);
        mhs[1].setNama("Edu");
        mhs[2] = new Mahasiswa(101);
        mhs[2].setNama("Icha");
        mhs[3] = new Mahasiswa(109);
        mhs[3].setNama("Aya");
        mhs[4] = new Mahasiswa(107);
        mhs[4].setNama("Kirana");

        System.out.println("Data : ");
        Larik.cetak(mhs);
        System.out.println();

        Mahasiswa cariMhs = new Mahasiswa(107);
        cariMhs.setNama("Kirana");

        int hasil = Larik.SequentialSearch(mhs, cariMhs);
        if (hasil >= 0) {
            System.out.println("Data " + mhs[hasil]);
            System.out.println("Ketemu di indeks ke-" + hasil);
        } else {
            System.out.println("Data yang dicari tidak ditemukan");
        }
    }
}
