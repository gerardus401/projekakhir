package Searching;

public class ObjectMhs {

    public static void main(String[] args) {
        Mahasiswa mhs[] = new Mahasiswa[3];
        mhs[0] = new Mahasiswa();
        mhs[0].setNama("Nidya");
        mhs[0].setNim(110);
        mhs[1] = new Mahasiswa();
        mhs[1].setNama("Velma");
        mhs[1].setNim(94);
        mhs[2] = new Mahasiswa();
        mhs[2].setNama("Cintya");
        mhs[2].setNim(100);

        Mahasiswa cariMhs = new Mahasiswa();
        cariMhs = new Mahasiswa();
        cariMhs.setNim(96);

        if (mhs[2].compareTo(cariMhs) == 0) {
            System.out.println("Sama");
        } else if (mhs[2].compareTo(cariMhs) == -1) {
            System.out.println("Lebih kecil");
        } else {
            System.out.println("Lebih besar");
        }
    }
}
//interpolasi, object sequential,object binarry, object interpolasi
