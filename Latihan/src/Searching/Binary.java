package Searching;

import java.util.Scanner;
import Nidya.Larik;
import java.util.Arrays;

public class Binary {

    static int[] larik = new int[5];
    static int kunci, indeksAwal, indeksAkhir;
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Langkah 0 : Baca data ke dalam larik dalam keadaan urut 
        larik[0] = 5;
        larik[1] = 3;
        larik[2] = 1;
        larik[3] = 9;
        larik[4] = 7;
        System.out.print("Data\t\t\t  : ");
        for (int i = 0; i < larik.length; i++) {
            System.out.print(larik[i] + " ");
        }
        System.out.println();
        Arrays.sort(larik);
        System.out.print("Data Setelah Diurutkan    : ");
        Larik.cetak(larik);
        System.out.println();
        // Langkah 1 :Baca data yang dicari ke dalam variabel kunci
        System.out.print("Data yang dicari\t  : ");
        kunci = sc.nextInt();
        System.out.println("Data " + kunci + " ketemu di indek ke : " 
        + Larik.binarySearch(larik, kunci));
    }
}
