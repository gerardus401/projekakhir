package Searching;

import java.util.Scanner;
import Nidya.Larik;

public class Sequential {

    static int[] larik = new int[5];
    static int kunci;
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Langkah 0 : Baca data ke dalam larik 
        larik[0] = 5;
        larik[1] = 3;
        larik[2] = 1;
        larik[3] = 9;
        larik[4] = 7;
        System.out.print("Data : ");
        for (int counter = 0; counter < larik.length; counter++) {
            System.out.print(larik[counter] + " ");
        }
        System.out.println();
        // Langkah 1 : Baca data yang dicari ke dalam variabel kunci
        System.out.print("Data yang dicari : ");
        kunci = sc.nextInt();
        System.out.println("Data " + kunci + " ketemu di indeks ke : " 
                + Larik.SequentialSearch(larik, kunci));
    }
}
