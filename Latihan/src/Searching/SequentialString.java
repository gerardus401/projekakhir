package Searching;

import Nidya.Larik;
import java.util.Scanner;

public class SequentialString {

    static String[] larik = new String[5];
    static String kunci;
    static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
        larik[0] = "Nidya";
        larik[1] = "Velma";
        larik[2] = "Cintya";
        larik[3] = "Tiansi";
        larik[4] = "Acha";
        System.out.println("Data : ");
        for (int i = 0; i < larik.length; i++) {
            System.out.println(larik[i] + " ");
        }
        System.out.println();
        System.out.print("Data yang dicari : ");
        kunci = sc.next();
        System.out.println("Data 1 ketemu di indeks ke : " + Larik.SequentialSearch(larik, kunci));
    }   
}
