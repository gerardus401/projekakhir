package StackStatis;

import java.util.NoSuchElementException;

public class stack {

    int[] elemen;
    int front;
    int size;

    public stack(int ukuran) {
        elemen = new int[ukuran];
        front = -1;
        size = 0;
    }

    public boolean push(int e) {
        if (size == elemen.length) {
            return false;
        } else {
            elemen[++front] = e;
            size++;
            return true;
        }
    }

    public boolean isEmpty() {
        if (front == -1) {
            return true;
        } else {
            return false;
        }
    }

    public int pop() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        } else {
            size--;
            return elemen[front--];
        }
    }
    
    public void toShow(){
        while(!isEmpty()){
            System.out.println(pop());
        }
    }
}
