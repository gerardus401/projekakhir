package SenaraiBerantaiObject;

public class MainObject {

    public static void main(String[] args) {
        Mahasiswa mhs1 = new Mahasiswa();
        mhs1.setNim(185314110);
        mhs1.setNama("Nidya");
        Mahasiswa mhs2 = new Mahasiswa();
        mhs2.setNim(185314094);
        mhs2.setNama("Velma");
        Mahasiswa mhs3 = new Mahasiswa();
        mhs3.setNim(185314080);
        mhs3.setNama("Cintya");
        Mahasiswa mhs4 = new Mahasiswa();
        mhs4.setNim(185314096);
        mhs4.setNama("Tiansi");
        Mahasiswa mhs5 = new Mahasiswa();
        mhs5.setNim(185314111);
        mhs5.setNama("Kenny");
        LinkedList test = new LinkedList();
        test.addFirst(mhs1);
        test.addLast(mhs2);
        test.addLast(mhs3);
        test.addLast(mhs4);
        test.addLast(mhs5);
        System.out.print("Hasil Sisip\t      : " + test.toString());
        test.removeFirst();
        System.out.println();
        System.out.print("Hasil Remove Pertama  : " + test.toString());
        test.removeLast();
        System.out.println();
        System.out.print("Hasil Remove Terkahir : " + test.toString());
    }
}
