package SenaraiBerantaiObject;

import java.util.NoSuchElementException;

public class LinkedList {

    private ListNode head;
    private int size;

    public LinkedList() {
        head = new ListNode();
        head.next = head;
        head.prev = head;
    }

    public ListNode getHead() {
        return head;
    }

    public void setHead(ListNode head) {
        this.head = head;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private ListNode addBefore(Object a, ListNode bantu) {
        ListNode baru = new ListNode(a);
        baru.next = bantu;
        baru.prev = bantu.prev;
        bantu.prev.next = baru;
        bantu.prev = baru;
        size++;
        return baru;
    }

    public void addFirst(Object x) {
        addBefore(x, head.next);
    }

    public void addLast(Object x) {
        addBefore(x, head);
    }

    private Object remove(ListNode bantu) {
        if (isEmpty()) {
            throw new NoSuchElementException();
        } else {
            bantu.prev.next = bantu.next;
            bantu.next.prev = bantu.prev;
            size--;
            return bantu.elemen;
        }
    }

    public Object removeFirst() {
        return remove(head.next);
    }

    public Object removeLast() {
        return remove(head.prev);
    }

    public boolean isEmpty() {
        if (head.next == head) {
            return true;
        } else {
            return false;
        }
    }

    public ListNode search(Object cari) {
        ListNode bantu = head.next;
        while (bantu != head) {
            if (bantu.elemen == cari) {
                return bantu;
            }
            bantu = bantu.next;
        }
        return null;
    }

    @Override
    public String toString() {
        String isi = " ";
        ListNode bantu = new ListNode();
        bantu = head.next;
        while (bantu != head) {
            if (bantu != head) {
                isi = isi + "  " + bantu.elemen;
                bantu = bantu.next;
            } else {
                isi = isi + "  " + bantu.elemen;
                bantu = bantu.next;
            }
        }
        isi = isi + " ";
        return isi;
    }
}
